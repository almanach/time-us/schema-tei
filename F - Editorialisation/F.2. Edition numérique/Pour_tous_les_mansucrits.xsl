<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY esp "&#160;">
    ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs tei"
    version="2.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>


    <!-- DECLARATION DES VARIABLES ULR UTILISEES DANS TOUT LE FICHIER XSLT -->

    <xsl:variable name="witfile">
        <xsl:value-of select="replace(base-uri(.), '.xml', '')"/>
    </xsl:variable>
    <xsl:variable name="pathAccueil">
        <xsl:value-of select="concat($witfile, '_accueil', '.html')"/>
    </xsl:variable>
    <xsl:variable name="indexmanuscrit">
        <xsl:value-of select="concat($witfile, '_index_manuscrits', '.html')"/>
    </xsl:variable>
    <xsl:variable name="informations">
        <xsl:value-of select="concat($witfile, '_informations_', '.html')"/>
    </xsl:variable>
    <xsl:variable name="prudhommes">
        <xsl:value-of select="concat($witfile, '_prudhommes_', '.html')"/>
    </xsl:variable>

    <!-- APPEL DU HEADER POUR L ECRASER  -->
    <xsl:template name="header" match="//teiHeader"/>

    <!-- Pages  -->
    <xsl:template name="Audiences" match="//text/body">
        <xsl:result-document href="{$pathAccueil}">
            <html>
                <head>

                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="projetTU.css"/>
                    <title>ACCUEIL</title>
                </head>
                <body>

                    <div class="barre">
                        <a class="link" href="{$pathAccueil}">Accueil</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$informations}">A propos de TIME US</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$prudhommes}">A propos des sources</a>
                        <xsl:text> | </xsl:text>
                    </div>

                    <img src="baniere.png" width="100%"/>


                    <div align="center">
                        <br/>
                        <a class="button" href="{$indexmanuscrit}">Index des manuscrits</a>&esp; </div>
                    <br/>

                    <h1 align="center">PROCÈS-VERBAUX DU CONSEIL DES PRUD'HOMMES DE PARIS <br/>-
                        PROJET TIME US -</h1>

                    <div class="accueil">
                        <p in="introp">Bienvenue sur la page d'accueil de l'éditorialisation des
                            sources manuscrites prud'hommales du <i>Projet Time Us.</i></p>
                        <p in="introp">Cette éditorialisation vous propose la transcription des
                            minutes des audiences du conseil des Prud'hommes de
                            Paris.<br/> Ces transcriptions ont été effectuées sur la plateforme
                            Transkribus puis extraites pour être structurées.</p>

                    </div>
                    <div class="accueil">
                        <h3>Informations supplémentaires :</h3>
                        <p in="introp">Ce projet vous est proposé par Victoria Le Fourner, stagiaire
                            de M2 pour le projet <i>Time Us</i>. Après avoir développé un schéma
                                <i>TEI</i> avec création de tests unitaires pour valider son
                            instanciation sur les fichiers du corpus, voici le prototype d'une
                            plate-forme de publication et visualisation du corpus</p>

                        <p in="introp">La transcription des compte-rendus est encore en cours. Il
                            est possible de consulter les mois suivants : <ul>Octobre 1847 (AD 75
                                D1U10 379): <a href="Texte/Octobre_1847.structured.xml">XML</a></ul>
                            <ul>Janvier à Mai 1848 (AD 75 D1U10 379) : <a
                                    href="Texte/Janvier_1848.structured.xml">XML</a></ul>
                            <ul>Janvier à Mars 1858 (AD 75 D1U10 386) : <a
                                    href="Texte/Janvier_1858.structured.xml">XML</a></ul>
                            <ul>Juin 1858 (AD 75 D1U10 386) : <a
                                    href="Texte/Juin_1858.structured.xml">XML</a></ul>
                            <ul>Janvier 1878 (AD 75 D1U10 405): <a
                                    href="Texte/Janvier_1878.structured.xml">XML</a></ul>
                            <ul>Juin 1878 (AD 75 D1U10 405) : <a
                                    href="Texte/Juin_1878.structured.xml">XML</a></ul>
                        </p>
                    </div>
                    
                    <div class="accueil">
                        Les fichiers ont été structurés selon un schéma ODD propre à Time Us. Celui-ci est disponible sur le <a href="https://gitlab.inria.fr/almanach/time-us/schema-tei/tree/master/ODD/ODD_minute"> GitLab de Time Us</a>.
                
                    </div>
                </body>
            </html>
        </xsl:result-document>
        <xsl:result-document href="{$informations}">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="projetTU.css"/>
                    <title>A propos du projet</title>
                </head>
                <body>

                    <div class="barre">
                        <a class="link" href="{$pathAccueil}">Accueil</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                    </div>

                    <img src="baniere.png" width="100%"/>
                    <br/>
                    <br/>
                    <div class="accueil">
                        <xsl:value-of select="//projectDesc"/>
                    </div>
                    <div class="accueil">
                        <xsl:text>Liens utiles :</xsl:text>
                        <div>
                            <a href="{'https://timeus.hypotheses.org/'}">Consulter le Blog
                                Hypothèse</a>
                        </div>
                        <div>
                            <a
                                href="{'http://timeusage.paris.inria.fr/mediawiki/index.php/Accueil'}"
                                >Consulter le Wiki</a>
                        </div>
                        <div>
                            <a href="{'https://gitlab.inria.fr/almanach/time-us'}">Consulter le
                                GitLab</a>
                        </div>

                    </div>

                </body>
            </html>
        </xsl:result-document>


        <xsl:result-document href="{$prudhommes}">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="projetTU.css"/>
                    <title>A propos des sources</title>
                </head>
                <body>
                    <div class="barre">
                        <a class="link" href="{$pathAccueil}">Accueil</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$informations}">A propos de TIME US</a>
                        <xsl:text> | </xsl:text>
                        
                    </div>

                    <img src="baniere.png" width="100%"/>
                    <br/>
                    <br/>
                    <div class="accueil">
                        <p>Les réformes judiciaires de l’an VIII ont confiées aux juges de paix la
                            résolution des conflits du travail dans les ateliers et les
                            manufactures. Ils avaient donc autorité sur les conflits liés aux
                            salaires, aux contrats, aux congés ou encore à la discipline. La loi du
                            18 mars 1806, restée en vigueur jusqu’en 1979, innove en instituant de
                            nouveaux juges, les conseillers prud’hommes, issus du monde des métiers
                            et qui ne sont donc pas des juristes professionnels. Les premiers
                            conseils sont institués en 1807 à Nîmes et Rouen. Cette juridiction
                            élective et paritaire était souhaitée depuis plusieurs années par les
                            manufacturiers et notamment par les soyeux lyonnais qui dénonçaient les
                            39 francs de frais de justice pris par les juges de paix. </p>
                        <p>La plupart du temps le maître refusait de venir devant le juge de paix
                            pour répondre aux réclamations de l’ouvrier. De plus, devant les bureaux
                            particuliers chargés de la conciliation, si une partie ne se présentait
                            pas le non-lieu était prononcé mais celui qui avait intenté le procès
                            devait payer les frais de justice. Il était en revanche difficile
                            d’imaginer qu’un maître refuserait de se présenter devant ses pairs,
                            élus conseillers des prud’hommes, au risque de se faire une mauvaise
                            réputation. L’établissement d’un conseil des prud’hommes a donc été
                            perçu comme plus équitable.</p>
                        <p>Le corps électoral était constitué à partir de 1810, des chefs d’atelier,
                            contremaîtres, teinturiers ainsi que d’ouvriers patentés. Ceux-ci
                            devaient savoir lire et écrire, et exercer leur activité depuis six ans
                            dans le ressort de la juridiction. Ils étaient élus pour trois ans. Les
                            ouvriers, compagnons et apprentis, bien que justiciables des conseils de
                            prud’hommes, n’y étaient ni électeurs, ni éligibles.</p>
                        <p>La compétence des conseillers des prud’hommes est triple : <ol>concilier
                                ou juger les différents entre fabricants et ouvriers ou entre chefs
                                d’atelier et compagnons ou apprentis ;</ol>
                            <ol>exercer un contrôle administratif sur le nombre des métiers
                                existants et sur le nombre d’ouvriers par manufacture ou
                                atelier ;</ol>
                            <ol>exercer un pouvoir de police en cas de désordre grave à l’audience
                                ou à l’atelier.</ol></p> 
                        <p> La mission
                            principale des prud’hommes est d’abord la conciliation à l’instar des juges
                            de paix. Le « bureau particulier » est créé à cet effet. En cas d’échec, le
                            « bureau général » prend le relais et rend ses jugements en présence des
                            deux tiers des conseillers et à la majorité des présents. Les prud’hommes
                            sont habilités à entendre des témoins et peuvent se rendre dans la
                            manufacture ou l’atelier pour examiner plus précisément une situation. En
                            novembre 1828 les conseillers prud’hommes deviennent des officiers publics.
                            A partir de 1848, tous les électeurs sont éligibles à condition d’être âgé
                            de 25 ans, de savoir lire et écrire et de résider depuis un an dans le
                            ressort du conseil.</p> </div>

                </body>
            </html>
        </xsl:result-document>



        <xsl:result-document href="{$indexmanuscrit}">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="projetTU.css"/>
                    <title>Manuscrits</title>
                </head>
                <body>
                    <!-- Barre de renvoi vers l'accueil et autres pages : présente sur toutes les pages-->
                    <div class="barre">
                        <a class="link" href="{$pathAccueil}">Accueil</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$informations}">A propos de TIME US</a>
                        <xsl:text> | </xsl:text>
                        <a class="link" href="{$prudhommes}">A propos des sources</a>
                        <xsl:text> | </xsl:text>
                    </div>
                    <!-- image pour l'accueil -->
                    <img src="baniere.png" width="100%"/>


                    <xsl:for-each select="//div[@type = 'manuscrit']">
                        <xsl:variable name="numeroManuscrit">
                            <xsl:number value="1 + count(preceding-sibling::div)" format="1"/>
                        </xsl:variable>
                        <ul>
                            <h3 align="center" class="index"><xsl:value-of select="@ana"/>.
                                <br/>(<xsl:value-of select="@source"/>)
                            </h3>
                        </ul>
                        <div align="right"><xsl:number
                                value="0 + count(descendant::div[@type = 'courtHearing'])"
                                format="1"/> audiences disponibles.</div>

                        <xsl:for-each select="div[@type = 'courtHearing']">

                            <xsl:variable name="numeroAudience">
                                <xsl:number value="1 + count(preceding-sibling::div)" format="1"/>
                            </xsl:variable>
                            <xsl:variable name="PageAffaire">
                                <xsl:value-of
                                    select="concat($witfile, '_audience_', $numeroAudience, '_affaire_''.html')"
                                />
                            </xsl:variable>



                            <xsl:result-document
                                href="{concat($witfile, '_manuscrit_',$numeroManuscrit,'_audience_', $numeroAudience,'.html')}">
                                <html>
                                    <head>
                                        <link rel="stylesheet" type="text/css" href="projetTU.css"/>
                                        <title>
                                            <xsl:value-of
                                                select="concat('_manuscrit_', $numeroManuscrit, '_audience_', $numeroAudience)"
                                            />
                                        </title>
                                    </head>
                                    <body>
                                        <div class="barre">
                                            <a class="link" href="{$pathAccueil}">Accueil</a>
                                            <xsl:text> | </xsl:text>
                                            <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                                            <xsl:text> | </xsl:text>
                                            <a class="link" href="{$informations}">A propos de TIME US</a>
                                            <xsl:text> | </xsl:text>
                                            <a class="link" href="{$prudhommes}">A propos des sources</a>
                                            <xsl:text> | </xsl:text>
                                        </div>

                                        <img src="baniere_3.png" width="100%"/>
                                        <h2 align="center">
                                            <xsl:value-of
                                                select="concat('Manuscrit n° ', $numeroManuscrit, ' - Audience n° ', $numeroAudience)"
                                            />
                                        </h2>

                                        <xsl:call-template name="audiences"/>
                                        <!--   PAGE POUR LES AFFAIRES            -->
                                        <xsl:for-each select="div[@type = 'case']">
                                            <xsl:variable name="numeroaffaire">
                                                <xsl:number
                                                  value="1 + count(preceding-sibling::div)"
                                                  format="1"/>
                                            </xsl:variable>
                                            <xsl:variable name="PageAffaires">
                                                <xsl:value-of
                                                  select="concat($witfile, '_audience_', $numeroAudience, '_affaire_''.html')"
                                                />
                                            </xsl:variable>


                                            <xsl:result-document
                                                href="{concat($witfile, '_manuscrit_',$numeroManuscrit,'_audience_',$numeroAudience,'_affaire_', $numeroaffaire,'.html')}">
                                                <html>
                                                  <head>
                                                  <link rel="stylesheet" type="text/css"
                                                  href="projetTU.css"/>
                                                  <title>
                                                  <xsl:value-of
                                                  select="concat('_manuscrit_', $numeroManuscrit, '_audience_', $numeroAudience, '_affaire_', $numeroaffaire)"
                                                  />
                                                  </title>
                                                  </head>
                                                  <body>
                                                      <div class="barre">
                                                          <a class="link" href="{$pathAccueil}">Accueil</a>
                                                          <xsl:text> | </xsl:text>
                                                          <a class="link" href="{$indexmanuscrit}">Index des manuscrits</a>
                                                          <xsl:text> | </xsl:text>
                                                          <a class="link" href="{$informations}">A propos de TIME US</a>
                                                          <xsl:text> | </xsl:text>
                                                          <a class="link" href="{$prudhommes}">A propos des sources</a>
                                                          <xsl:text> | </xsl:text>
                                                      </div>

                                                  <img src="baniere.png" width="100%"/>
                                                  <h2 align="center">
                                                  <xsl:value-of
                                                  select="concat('Audience n° ', $numeroAudience, ' - Affaire n° ', $numeroaffaire)"
                                                  />
                                                  </h2>
                                                  <xsl:call-template name="affaires"/>
                                                  </body>
                                                </html>
                                            </xsl:result-document>

                                            <ul class="LinkAudience">
                                                <a
                                                  href="{concat($witfile,'_manuscrit_',$numeroManuscrit, '_audience_',$numeroAudience,'_affaire_',$numeroaffaire,'.html')}"
                                                  ><xsl:value-of
                                                  select="concat('Audience n°', $numeroAudience, ' - Affaire n°', $numeroaffaire)"
                                                  /></a>
                                            </ul>

                                        </xsl:for-each>

                                    </body>
                                </html>
                            </xsl:result-document>
                            <br/>
                            <div>
                                <xsl:if test="./note[@type = 'date_audience']">
                                    <ul>
                                        <h3><xsl:copy-of select="./note[@type = 'date_audience']"/>
                                            :</h3>
                                    </ul>
                                </xsl:if>

                                <ul class="lienAudience">
                                    <a
                                        href="{concat($witfile, '_manuscrit_',$numeroManuscrit,'_audience_',$numeroAudience,'.html')}"
                                        ><xsl:value-of
                                            select="concat('Manuscrit n°', $numeroManuscrit, ' - Audience n°', $numeroAudience)"
                                        /></a>
                                </ul>
                            </div>
                            <button onclick="topFunction()" id="myBtn"
                                title="Retour vers le haut de page"
                                style="position:fixed;bottom:20px;right:30px;z-index:99;padding:5px;font-size:14px;"
                                >Haut de page</button>

                            <script>function topFunction() {
                                document.body.scrollTop = 0; // For Safari
                                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                                }
                            </script>
                        </xsl:for-each>
                        <a href="{$witfile,'_manuscrit',$numeroManuscrit}"/>
                    </xsl:for-each>
                </body>
            </html>
        </xsl:result-document>

    </xsl:template>



    <xsl:template name="audiences">

        <h3 align="right">
            <xsl:copy-of select="note[@type = 'date_audience']"/>
        </h3>
        <br/>
        <div class="accueil">
            <xsl:copy-of select="opener"/>
        </div>
    </xsl:template>

    <xsl:template name="affaires">
        <div class="accueil">
            <u>
                <b>
                    <xsl:text>Demandeur :</xsl:text>
                </b>
            </u>
            <xsl:value-of select="note[@type = 'partie1']"/>
            <br/>
            <u>
                <b>
                    <xsl:text>Défendeur :</xsl:text>
                </b>
            </u>
            <xsl:value-of select="note[@type = 'partie2']"/>
            <br/>
            <xsl:if test="opener">
                <b>
                    <u>
                        <xsl:text>Date de la séance :</xsl:text>
                    </u>
                </b>
                <xsl:copy-of select="opener"/>
                <br/>
            </xsl:if>
        </div>
        <div class="div_core">

            <xsl:if test="div[@type = 'identificationParties']">
                <br/>
                <div align="center">
                    <b>
                        <xsl:text>Identification des parties du procès</xsl:text>
                    </b>
                </div>
                <br/>
                <div align="justify">
                    <xsl:copy-of select="div[@type = 'identificationParties']"/>
                </div>
            </xsl:if>
            <xsl:if test="div[@type = 'pointDeFait']">
                <br/>
                <div align="center">
                    <b>
                        <xsl:text>Point de Fait</xsl:text>
                    </b>
                </div>
                <br/>
                <div align="justify">
                    <xsl:copy-of select="div[@type = 'pointDeFait']"/>
                </div>
            </xsl:if>
            <xsl:if test="div[@type = 'pointDeDroit']">
                <br/>
                <div align="center">
                    <b>
                        <xsl:text>Point de Droit</xsl:text>
                    </b>
                </div>
                <br/>
                <div align="justify">
                    <xsl:copy-of select="div[@type = 'pointDeDroit']"/>
                </div>
            </xsl:if>
            <xsl:if test="div[@type = 'judgment']">
                <br/>
                <div align="center">
                    <b>
                        <xsl:text>Jugement</xsl:text>
                    </b>
                </div>
                <br/>
                <div align="justify">
                    <xsl:copy-of select="div[@type = 'judgment']"/>
                </div>
            </xsl:if>
            <xsl:if test="div[@type = 'reste']">
                <br/>
                <div align="center">
                    <b>
                        <xsl:text>Texte non detecté :</xsl:text>
                    </b>
                </div>
                <br/>
                <div align="justify">
                    <xsl:copy-of select="div[@type = 'reste']"/>
                </div>
            </xsl:if>
            <button onclick="topFunction()" id="myBtn" title="Retour vers le haut de page"
                style="position:fixed;bottom:20px;right:30px;z-index:99;padding:5px;font-size:14px;"
                >Haut de page</button>

        </div>

        <script>function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }
        </script>
    </xsl:template>


</xsl:stylesheet>
