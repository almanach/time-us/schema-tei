from bs4 import BeautifulSoup
import re
import os
import argparse

# définition des constantes
PATTERN_CESURE = re.compile(r"[¬-]+[ \n]+")
PATTERN_DIV_COURTHEARING =re.compile(r"((?=(Audience)).*?((?= Audience|$)))")
PATTERN_DIV_CASE = re.compile(r"(Du jour|Du dit jour|Dudit jour |Du dit v|Entre).*?((?=(Du jour|Du dit jour|Du dit v|Dudit jour))|(?=( dessus\. Du|$)))")
PATTERN_OPENER = re.compile(r"(Du jour|Du dit jour|Dudit jour |Du dit v).*?(?=(Entre|Monsieur))")
Pattern_identification_parties = re.compile(r"(?=Entre).*?(?=(Point de fait|Point De fait|Point de Fait))")
Pattern_Point_de_fait = re.compile(r"((?=Point de fait|Point De fait|Point de Fait).*?(?=(Point de d|Point de D|Pont de d)))")
Pattern_Point_de_droit = re.compile(r"((?=(Point de d|Point de D|Pont de doit)).*?(?=(Après avoir entendu|après avoir entendu)))")
Pattern_Jugement = re.compile(r"((?=Après avoir entendu|après avoir entendu).*?(?=$))")
Pattern_opener_audience = re.compile(r"(Audience .*?(?=Entre))")
Blanc = re.compile(r' {1,}')
Partie_1 = re.compile(r"(?<=Entre).*?(?=demeurant)")
Partie_2 = re.compile(r"(?<=D'une part ).*?(?=demeurant)")
Partie_2_autre_possibilite = re.compile(r"(?<=Et).*?(?=demeurant)")
Partie_2_autre_possibilite1 = re.compile(r"(?<=d'une part).*?(?=demeurant)")
Date_audience = re.compile(r"(?<=Audience du ).*?(?=si|Si)")

# définitions des fonctions

def delete_facs(soup):
    for tag in (soup.find_all('p')):
        del tag['facs']
       # print(tag.attrs)
    return soup

def delete_facsimile(soup):
    for tag in soup.find_all('facsimile'):
        tag.decompose()
    return soup


def delete_cesure(soup):
    for element in soup.find_all('p'): #trouve tous les p
        jointure = "".join(element.get_text())
        element.string = re.sub(PATTERN_CESURE,"", jointure)
        # print(element)
    return soup


def ajout_balises_div_type_manuscrit_body(soup):
        forme_originale = soup.body
        forme_originale.name = "div"
        forme_originale['type'] = "manuscrit"

        forme_originale.wrap(soup.new_tag("body"))
        return soup


def supprimer_lb(soup):
    for tag in soup.find_all('lb'):
        useless_lb = tag.extract()
    return soup



def supprimer_p(soup):
    soup.body.name = "placeholder_body"
    miso = BeautifulSoup("<p></p>", "xml")  # je cree nouvel arbre
    for element in soup.placeholder_body.find_all('p'):  # ne prend pas les p du header
        extraction_element = element.string.extract()
        miso.p.append(extraction_element)
    soup.placeholder_body.clear()
    tag_body = miso.new_tag("body")
    soup.placeholder_body.insert_after(tag_body)
    soup.body.insert(1, miso)
    soup.placeholder_body.decompose()

    all_content_in_one_line = ""
    for content in soup.body.p.contents:
        all_content_in_one_line = all_content_in_one_line + content
        all_content_in_one_line = all_content_in_one_line.replace('\n', ' ')
        all_content_in_one_line = re.sub(Blanc,' ', all_content_in_one_line)
    soup.body.p.clear()
    soup.body.p.string = all_content_in_one_line
    return soup


def insert_audiences(soup):
    audience = []
    audiences = []

    for m in re.finditer(PATTERN_DIV_COURTHEARING, soup.get_text()):
        audience = []
        audiences.append(audience)
        audience.append(m.group(1))
    soup.body.p.clear()
    for element_audience in audiences:
        audience_str = ''.join(element_audience)
        miso = BeautifulSoup('<placeholder>xxxxxx</placeholder>', "xml")
        miso.placeholder.string.replace_with(audience_str)
        soup.body.div.append(miso)

    for element in soup.find_all('placeholder'):
        element.name = 'div'
        element['type'] = "courtHearing"

    else:
        return soup
    return soup

def insert_case(soup):
    for audience in soup.find_all('div',type='courtHearing'):
        affaires = []
        opener_audiences = []
        dateS = []
        for y in re.finditer(Pattern_opener_audience,audience.get_text()):
            opener_audience = []
            opener_audiences.append(opener_audience)
            opener_audience.append(y.group(0))

        for m in re.finditer(PATTERN_DIV_CASE, audience.get_text()):
            affaire = []
            affaires.append(affaire)
            affaire.append(m.group(0))

        for o in re.finditer(Date_audience, audience.get_text()):
            date = []
            dateS.append(date)
            date.append(o.group(0))
        audience.clear()

        for element_opener in opener_audiences:
            audience_str = ''.join(element_opener)
            potage = BeautifulSoup('<placeholder6>xxxx</placeholder6>', "xml")
            potage.placeholder6.string.replace_with(audience_str)
            audience.append(potage)


        for element_affaire in affaires:
            audience_str = ''.join(element_affaire)
            miso = BeautifulSoup('<placeholder>xxxxx</placeholder>', "xml")
            miso.placeholder.string.replace_with(audience_str)
            audience.append(miso)

        for element_date in dateS:
            audience_str = ''.join(element_date)
            miso = BeautifulSoup('<date>xxxx</date>', "xml")
            miso.date.string.replace_with(audience_str)
            audience.append(miso)

    for element in soup.find_all('placeholder'):
        element.name = 'div'
        element['type'] = "case"

    for element in soup.find_all('poubelle'):
        element.name = 'opener'

    for element in soup.find_all('date'):
        element.name = 'note'
        element['type'] = "date_audience"


    else:
        return soup
    return soup


def insert_segmentation_interne_aux_affaires(soup):
    c = 0
    toutes_les_affaires = soup.find_all('div',type='case')


    for ensemble_affaire in soup.find_all('div',type='case'):
        texte_ensemble_affaire = ensemble_affaire.get_text()
        parties_tous_proces = []
        pointDeFaitS = []
        pointDeDroitS =[]
        jugementS = []
        openerS_affaire = []
        restes = []

        for poi in re.finditer(PATTERN_OPENER, texte_ensemble_affaire):

            opener_affaire= []
            openerS_affaire.append(opener_affaire)
            opener_affaire.append(poi.group(0))


        for pz in re.finditer(Pattern_identification_parties,texte_ensemble_affaire):
            partie_un_proces = []
            parties_tous_proces.append(partie_un_proces)
            partie_un_proces.append(pz.group(0))


        for wp in re.finditer(Pattern_Point_de_fait, texte_ensemble_affaire):
            pointDeFait = []
            pointDeFaitS.append(pointDeFait)
            pointDeFait.append(wp.group(0))

        for mp in re.finditer(Pattern_Point_de_droit, texte_ensemble_affaire):
            pointDeDroit = []
            pointDeDroitS.append(pointDeDroit)
            pointDeDroit.append(mp.group(0))


        for vi in re.finditer(Pattern_Jugement, texte_ensemble_affaire):
            jugement = []
            jugementS.append(jugement)
            jugement.append(vi.group(0))
        copie_ensemble_texte = texte_ensemble_affaire[:]

        segements = jugementS+pointDeDroitS+pointDeFaitS+parties_tous_proces + openerS_affaire
        for segment in segements:
            for element_segment in segment:
                if len(element_segment) > 0:
                    copie_ensemble_texte = copie_ensemble_texte.replace(element_segment,"")

        if len(copie_ensemble_texte)>0:
            reste = []
            restes.append(reste)
            reste.append(copie_ensemble_texte)


        ensemble_affaire.clear()

        for element_opener in openerS_affaire:
            opener_str = ''.join(element_opener)
            arbre1 = BeautifulSoup('<placeholder8>xxxx</placeholder8>', "xml")
            arbre1.placeholder8.string.replace_with(opener_str)
            ensemble_affaire.append(arbre1)

        for element_parties in parties_tous_proces:
            audience_str = ''.join(element_parties)
            arbre2 = BeautifulSoup('<placeholder7>xxxxx</placeholder7>', "xml")
            arbre2.placeholder7.string.replace_with(audience_str)
            ensemble_affaire.append(arbre2)

        for element_ptFait in pointDeFaitS:
            fait_str = ''.join(element_ptFait)
            arbre3 = BeautifulSoup('<placeholder6>xxxx</placeholder6>', "xml")
            arbre3.placeholder6.string.replace_with(fait_str)
            ensemble_affaire.append(arbre3)

        for element_ptDroit in pointDeDroitS:
            droit_str = ''.join(element_ptDroit)
            arbre4 = BeautifulSoup('<placeholder5>xxxxxx</placeholder5>', "xml")
            arbre4.placeholder5.string.replace_with(droit_str)
            ensemble_affaire.append(arbre4)

        for element_jugement in jugementS:
            jug_str = ''.join(element_jugement)
            arbre5 = BeautifulSoup('<placeholder4>xxxxxxx</placeholder4>', "xml")
            arbre5.placeholder4.string.replace_with(jug_str)
            ensemble_affaire.append(arbre5)

        for element_reste in restes:
            reste_str = ''.join(element_reste)
            arbre6 = BeautifulSoup('<placeholder3>xxxxxxx</placeholder3>', "xml")
            arbre6.placeholder3.string.replace_with(reste_str)
            ensemble_affaire.append(arbre6)

    for p3 in soup.find_all('placeholder3'):
        p3.name = 'p'
        p3.wrap(soup.new_tag("div", type='reste'))


    for p8 in soup.find_all('placeholder8'):
        p8.name = 'opener'

    for p7 in soup.find_all('placeholder7'):
        p7.name = 'p'
        p7.wrap(soup.new_tag("div", type='identificationParties'))



    for p6 in soup.find_all('placeholder6'):
        p6.name = 'p'
        p6.wrap(soup.new_tag("div", type='pointDeFait'))

    for p5 in soup.find_all('placeholder5'):
        p5.name = 'p'
        p5.wrap(soup.new_tag("div", type='pointDeDroit'))

    for p4 in soup.find_all('placeholder4'):
        p4.name = 'p'
        p4.wrap(soup.new_tag("div", type='judgment'))

    else:
        return soup
    return soup

def insert_n_audience(soup):
    c = 1
    for audience in soup.find_all('div', type='courtHearing'):
        audience['n']= c
        c =c+1
    return soup

def insert_n_affaires(soup):
    c = 1
    for affaire in soup.find_all('div', type='case'):
        affaire['n']= c
        c =c+1
    return soup

def identificationPartiesEtDateAudience(soup):
    for affaire in soup.find_all('div',type='case'):
        partie1S = []
        partie2S = []

        for y in re.finditer(Partie_1,affaire.get_text()):
            partie1 = []
            partie1S.append(partie1)
            partie1.append(y.group(0))


        for m in re.finditer((Partie_2 and Partie_2_autre_possibilite and Partie_2_autre_possibilite1), affaire.get_text()):
            partie2 = []
            partie2S.append(partie2)
            partie2.append(m.group(0))

        for element_partie1 in partie1S:
            audience_str = ''.join(element_partie1)
            potage = BeautifulSoup('<placeholder1>xxxxxx</placeholder1>', "xml")
            potage.placeholder1.string.replace_with(audience_str)
            affaire.append(potage)

        for element in soup.find_all('poubelle'):
            element.name = 'note'
            element['type']='partie1'

        for element_partie2 in partie2S:
            audience_str = ''.join(element_partie2)
            potage = BeautifulSoup('<placeholder>victoria</placeholder>', "xml")
            potage.placeholder.string.replace_with(audience_str)
            affaire.append(potage)

        for element in soup.find_all('placeholder'):
            element.name = 'note'
            element['type'] = 'partie2'

    else:
        return soup
    return soup


# script principal

parser = argparse.ArgumentParser(description="Transforme les fichiers pour être en conformité avec la structure de l'ODD définie pour le projet Time US.")

parser.add_argument("-i", "--input", action="store", nargs=1, default=["input"], help="path to directory containing file to transform")
parser.add_argument("-o", "--output", action="store", nargs=1, default=["output"], help="path to directory receiving transformed files.")
CWD = os.path.dirname(os.path.abspath(__file__))

args = parser.parse_args()
PATH_TO_INPUT = os.path.join(CWD, args.input[0])
PATH_TO_OUTPUT = os.path.join(CWD, args.output[0])


try:
    input_content_l = os.listdir(PATH_TO_INPUT)
    if len(input_content_l) == 0:
        print("Rien à transformer dans le dossier.")
    else:
        input_content_l[:] = (value for value in input_content_l if value != ".DS_Store")
        for document in input_content_l:


                path_to_document = os.path.join(PATH_TO_INPUT, document)
                path_to_document_out = os.path.join(PATH_TO_OUTPUT,"%s.structure"% document)

                with open(path_to_document) as fp:
                        text = fp.read()
                        soup = BeautifulSoup(text, "xml")
                        facs = delete_facs(soup)
                        sans_facs = delete_facsimile(facs)
                        sauts_ligne = supprimer_lb(sans_facs)

                        cesure = delete_cesure(sauts_ligne)
                        PARAGRAPHE = supprimer_p(cesure)
                        manuscrit = ajout_balises_div_type_manuscrit_body(cesure)
                        audiences = insert_audiences(manuscrit)
                        affaires = insert_case(audiences)
                        numero = insert_n_audience(affaires)
                        numeroaffaire = insert_n_affaires(numero)
                        afinal= insert_segmentation_interne_aux_affaires(numeroaffaire)
                        final = identificationPartiesEtDateAudience(afinal)


                with open(path_to_document_out, "w") as f:
                    f.write(str(final))
except Exception as e:
    print(e)
