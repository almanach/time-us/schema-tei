from bs4 import BeautifulSoup
import re
import os
import argparse

# définition des constantes
PATTERN_CESURE = re.compile(r"[¬-]+[ \n]+")
PATTERN_NOM_JOURNAL_DANS_TITLE = re.compile(r"^([\w' ]+?),")
PATTERN_DATE_PUBLICATION_JOURNAL = re.compile(r'publication ?=( "|")([\d\/]+)"')
PATTERN_DIV_COURTHEARING =re.compile(r"((Audience|Séance)[du ]+\d+.+)")
PATTERN_DIV_COURTHEARING_JOUR = re.compile(r"((Lundi|Mardi|Mercredi|Jeudi|Vendredi)+ \d+.+,)")


# définitions des fonctions

# 2. Supprimer les facs  = Faire un fichier propre mais conservation des lb pour pouvoir trouver les lignes dans la suite du script
def delete_facs(soup):
    for tag in (soup.find_all('p')):
        del tag['facs']
       # print(tag.attrs)
    return soup

# fonction pour supprimer les césures moches dans les p contenus dans soup
def delete_cesure(soup):
    for element in soup.find_all('p'): #trouve tous les p
        element.string = re.sub(PATTERN_CESURE,"", element.get_text())
    return soup

def ajouter_head(soup):
    # patate = (list(soup.body.p)[2])# utiliser contents plutot?
    # patate.wrap(soup.new_tag('head'))

    paragraphe_dans_body = soup.body.p
    paragraphe_dans_body.name = "head"
    return soup


def ajout_balises_div_type_section_et_type_newspaper_body(soup):
        forme_originale = soup.body
        forme_originale.name = "div"
        forme_originale['type'] = "section"
        # ajout de l'attribut type section mais le faire pour body AVANT de le changer en div

        div_newspaper = forme_originale.wrap(soup.new_tag("div"))
        div_newspaper['type'] = "newspaper"

        div_newspaper.wrap(soup.new_tag("body"))
        return soup


    # Supprimer les lb dans le texte mais commenter tant que les lignes sont utiles pour compter
def supprimer_lb(soup):
    for tag in soup.find_all('lb'):
        useless_lb = tag.extract()
    return soup

def creation_du_front_a_ajouter(soup):
    miso = BeautifulSoup('<title>xxxx</title>',"xml")

    # création de l'enchainement de balise dans le nouvel arbre
    miso.title.wrap(miso.new_tag("bibl"))
    miso.bibl.wrap(miso.new_tag("listBibl"))
    miso.listBibl.wrap(miso.new_tag("front"))
    miso.bibl.title.insert_after(miso.new_tag("date"))

    # insertion du nouvel arbre dans l'arbre soup de base
    soup.body.insert_before(miso)

    texte_dans_titre = soup.titleStmt.title.get_text()

    result = re.search(PATTERN_NOM_JOURNAL_DANS_TITLE,texte_dans_titre)
    # print(result)
    if result:
        resultat = result.group(0) #le nom du journal
    soup.listBibl.title.string.replace_with(resultat)

    texte_du_sourceDesc = soup.teiHeader.sourceDesc.get_text()
    result_date_journal = re.search(PATTERN_DATE_PUBLICATION_JOURNAL,texte_du_sourceDesc)

    if result_date_journal:
        resultat_date = result_date_journal.group(2) #la date du journal avec des slash

    soup.date['when-iso'] = resultat_date
    soup.date.string = resultat_date

    return soup

def insertion_div_type_audience(soup):

    audience_pattern = re.findall((PATTERN_DIV_COURTHEARING),soup.get_text())
    audience_pattern_autre = re.findall((PATTERN_DIV_COURTHEARING_JOUR),soup.get_text())

    if len(audience_pattern) >= 1 or len(audience_pattern_autre)>=1:
        audience=[]
        audiences = []
        paragraphe = front.body.find_all("p")
        for element in paragraphe:
            trouver_la_regex = re.search(PATTERN_DIV_COURTHEARING, element.string)
            trouver_la_regex_date = re.search(PATTERN_DIV_COURTHEARING_JOUR,element.string)
            if trouver_la_regex or trouver_la_regex_date:
                audience = []
                audiences.append(audience)
            audience.append(element)

        for element_audience in audiences:
            div = soup.new_tag("placeholder")
            #print(type(element_audience[0]))
            element_audience[0].insert_before(div)
            ancre= element_audience[0].find_previous_sibling('placeholder')


            for para in element_audience:
                extraction_para = para.extract()
                ancre.append(extraction_para)

        for element in soup.find_all('placeholder'):
            element.name = 'div'
            element['type']="courtHearing"

    else :
        return soup
    return soup

# script principal
parser = argparse.ArgumentParser(description="Transforme les fichiers pour être en conformité avec la structure de l'ODD définie pour le projet Time US.")

parser.add_argument("-i", "--input", action="store", nargs=1, default=["input"], help="path to directory containing file to transform")
parser.add_argument("-o", "--output", action="store", nargs=1, default=["output"], help="path to directory receiving transformed files.")
CWD = os.path.dirname(os.path.abspath(__file__))

args = parser.parse_args()
PATH_TO_INPUT = os.path.join(CWD, args.input[0])
PATH_TO_OUTPUT = os.path.join(CWD, args.output[0])


try:
    input_content_l = os.listdir(PATH_TO_INPUT)
    if len(input_content_l) == 0:
        print("Rien à transformer dans le dossier.")
    else:
        input_content_l[:] = (value for value in input_content_l if value != ".DS_Store")
        for document in input_content_l:


                path_to_document = os.path.join(PATH_TO_INPUT, document)
                path_to_document_out = os.path.join(PATH_TO_OUTPUT,"%s.xml"% document)
                #path_to_document_out = os.path.join(PATH_TO_OUTPUT, "%s.xml" % document)

                with open(path_to_document) as fp:
                    text = fp.read()
                    soup = BeautifulSoup(text, "xml")
                    facs = delete_facs(soup)
                    titre = ajouter_head(facs)
                    corps = ajout_balises_div_type_section_et_type_newspaper_body(titre)
                    sauts_ligne = supprimer_lb(corps)
                    cesure = delete_cesure(sauts_ligne)
                    front = creation_du_front_a_ajouter(cesure)
                    courtHearing = insertion_div_type_audience(front)
                    final = courtHearing.prettify()



                with open(path_to_document_out, "w") as f:
                    f.write(final)
except Exception as e:
    print(e)
