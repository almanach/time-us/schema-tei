Ce dossier contient les annexes du mémoire de stage pour le Master "Technologies numériques appliquées à l'histoire" de l'École nationale des chartes, intitulé *Étude de la structuration automatique et de l'éditorialisaion d'un corpus hétérogène. L'exemple des sources du conseil des prud'hommes pour le textile du XIXème siècle du projet Time Us.*.

- `A - Sources` : contient des exemples de documents d'archives utilisés dans le cadre du projet Time Us, et des documents de contexte sur ces archives.

- `B - Outils` : contient des scripts ScanTailor ainsi que des captures d'écran de l'interface graphique des outils utilisés dans le cadre du projet Time Us: Transkribus, eScriptorium et ShareDocs.

- `C - Annotation manuelle` : contient des sets de fichiers tests pour l'annotation manuelle et de la documentation en vue de la réalisation du schéma de validation.

- `D - Schéma de validation` : contient les schémas ODD pour les différentes sources du projet (minutes et presse) ainsi que des éléments pour le fonctionnement du set personnalisé sur Transkribus.

- `E - Structuration automatique`: contient les scripts python pour automatiser la structuration et l'annotation des sources.

- `F - Editorialisation` : contient des captures d'écran de la publication numérique des minutes structurées ainsi que les fichiers utilisés pour cette éditorialisation.
